import { 
  CATEGORY_SELECT_CARD,
  CATEGORY_CREATE_AGRICULTURALMACHINERY,
  CATEGORY_REMOVE_AGRICULTURALMACHINERY,
  CATEGORY_CREATE_POULTRY_AND_LIVESTOCK,
  CATEGORY_REMOVE_POULTRY_AND_LIVESTOCK,
  CATEGORY_CREATE_IRRIGATION_SYSTEM,
  CATEGORY_REMOVE_IRRIGATION_SYSTEM
} from './ActionType';

export const setSelectedCardAction = (cardKey) => (dispatch) => {
    dispatch({
      type: CATEGORY_SELECT_CARD,
      payload: cardKey
    });
};

export const createAgricultralMachineryAction = (value) => (dispatch) => {
  dispatch({
    type: CATEGORY_CREATE_AGRICULTURALMACHINERY,
    payload: value
  });
};

export const removeAgricultralMachineryAction = (index) => (dispatch) => {
  dispatch({
    type: CATEGORY_REMOVE_AGRICULTURALMACHINERY,
    payload: index
  });
};

export const createLivestockAction = (value) => (dispatch) => {
  dispatch({
    type: CATEGORY_CREATE_POULTRY_AND_LIVESTOCK,
    payload: value
  });
};

export const removeLivestockAction = (index) => (dispatch) => {
  dispatch({
    type: CATEGORY_REMOVE_POULTRY_AND_LIVESTOCK,
    payload: index
  });
};

export const createIrrigationSystemAction = (value) => (dispatch) => {
  dispatch({
    type: CATEGORY_CREATE_IRRIGATION_SYSTEM,
    payload: value
  });
};

export const removeIrrigationSystemAction = (index) => (dispatch) => {
  dispatch({
    type: CATEGORY_REMOVE_IRRIGATION_SYSTEM,
    payload: index
  });
};