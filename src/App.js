import React, { useState } from "react";
import { BrowserRouter as Router, Routes ,Route } from "react-router-dom";
import './assets/sass/main.scss';
import Sider from 'components/Sider';
import Header from 'components/Header';
import Operation from 'components/Operation';
import Footer from 'components/Footer';
import {
  ROUTE_ROOT,
  ROUTE_DASHBOARD,
  ROUTE_EVENTS,
  ROUTE_ANALYTICS,
  ROUTE_OPERATION,
  ROUTE_SETTINGS,
  ROUTE_HELP,
  ROUTE_TERMS_OF_USE,
  ROUTE_PRIVACY_POLICY,
  ROUTE_SUPPORT
} from 'routes';

function App() {
  const [inactive, setInactive] = useState(false);

  return (
    <div className="App">
      <Router>
        <div className={`container ${inactive ? "inactive" : ""}`}>
          <Sider />
          <div className='content'>
            <Header />
                <Routes>
                  <Route key='root' path={ROUTE_ROOT} element={<h1>Dashboard</h1>} />
                  <Route key='dashboard' path={ROUTE_DASHBOARD} element={<h1>Dashboard</h1>} />
                  <Route key='events' path={ROUTE_EVENTS} element={<h1>Events</h1>} />
                  <Route key='analytics' path={ROUTE_ANALYTICS} element={<h1>Analytics</h1>} />
                  <Route key='operation' path={ROUTE_OPERATION(':tab')} element={<Operation />} />
                  <Route key='settings' path={ROUTE_SETTINGS} element={<h1>Settings</h1>} />
                  <Route key='help' path={ROUTE_HELP} element={<h1>Help</h1>} />
                  <Route key='settings' path={ROUTE_TERMS_OF_USE} element={<h1>Terms of Use</h1>} />
                  <Route key='settings' path={ROUTE_PRIVACY_POLICY} element={<h1>Privacy Policy</h1>} />
                  <Route key='settings' path={ROUTE_SUPPORT} element={<h1>Support</h1>} />
                </Routes>
            <Footer />
          </div>
      </div>
      </Router>
    </div>
  );
}

export default App;
