export const DASHBOARD = 'Dashboard';
export const EVENTS = 'Events';
export const ANALYTICS = 'Analytics';
export const OPERATION = 'Operation';
export const SETTINGS = 'Settings';
export const MEETING_SCHEDULER = 'Meeting scheduler';
export const CAMPAIGN = 'Campaign';

// Categories manager
export const CATEGORIES_MANAGER = 'Categories manager';
export const MATERIAL = 'Raw Materials, Chemicals, Paper, Fuel';
export const EQUIPMENT = 'Industrial Equipment & Tools';
export const COMPONENTS = 'Components & Supplies';
export const CONSTRUCTION = 'Construction, Transportation & Facility Equipment...';
export const MEDICAL = 'Medical, Laboratory & Test Equipment & Supplies & Pharm...';
export const FOOD = 'Food, Cleaning & Service Industry Equipment...';

export const COPYRIGHT = 'Copyright © 2017-2021 AladdinB2B - Connecting Businesses';
export const TERMS_OF_USE = 'Terms of Use';
export const PRIVACY_POLICY = 'Privacy Policy';
export const SUPPORT = 'Support';

export const AGRICULTURAL_MACHINERY_TREE_TITLE = 'Agricultural machinery for soil preparation';
export const ADD_YOUR_OPTION = 'Add your option';
export const ADD_OPTION = 'Add option';
export const CREATE_OPTION_MSG = 'Option added successfully';
export const ADD = 'Add';
export const CANCEL = 'Cancel';
export const FIELD_IS_REQUIRED = 'This field is required';
export const OPTION_NAME = 'Option name';
export const POULTRY_AND_LIVESTOCK = 'Poultry and livestock';
export const IRRIGATION_SYSTEMS = 'Irrigation systems and equipment';