import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom'
import { Menu } from 'antd';
import LayoutTopFillIcon from 'remixicon-react/LayoutTopFillIcon';
import LayoutMasonryFillIcon from 'remixicon-react/LayoutMasonryFillIcon';
import CalendarTodoFillIcon from 'remixicon-react/CalendarTodoFillIcon';
import BarChartFillIcon from 'remixicon-react/BarChartFillIcon';
import SlideshowFillIcon from 'remixicon-react/SlideshowFillIcon';
import Settings4FillIcon from 'remixicon-react/Settings4FillIcon';
import QuestionLineIcon from 'remixicon-react/QuestionLineIcon';
import ArrowRightSLineIcon from 'remixicon-react/ArrowRightSLineIcon';

import { i18n } from 'config';
import {
    ROUTE_DASHBOARD,
    ROUTE_EVENTS,
    ROUTE_ANALYTICS,
    ROUTE_OPERATION,
    ROUTE_SETTINGS,
    ROUTE_HELP
} from 'routes';

const { SubMenu } = Menu;
const Sider = () => {
    const location = useLocation();
    const [selectedMenu, setSelectedMenu] = useState([]);

    useEffect(() => {
        const locations = location.pathname.split('/');
        setSelectedMenu([locations[locations.length - 1]]);
    }, [location]);

    const handleSelect = (menu) => {
        var index = menu.item.props.tabIndex;
        document.getElementById('sider_ink').style.top = (index * 6) + 'rem';
    }

    window.addEventListener('load', () => {
        let selectedMenu = document.getElementsByClassName('ant-menu-item-selected');
        if (!selectedMenu.length) {
            selectedMenu = document.getElementsByClassName('ant-menu-submenu-selected');
        }
        if (selectedMenu.length) { 
            const index = selectedMenu[0].getAttribute('tabindex');
            document.getElementById('sider_ink').style.top = (index * 6) + 'rem';
            document.getElementById('sider_ink').style.opacity = 1;
        }
    });

    const handleChageFloat = () => {

    };

    return (
        <div className='sider'>
            <div className='layout_link'>
                <LayoutTopFillIcon size={18}/>
            </div>
            
            <Menu selectedKeys={selectedMenu} onSelect={handleSelect}>
                <div id='sider_ink' />
                <Menu.Item key='dashboard' tabIndex={0}>
                    <Link to={ROUTE_DASHBOARD} className={`menu-item`} >
                        <LayoutMasonryFillIcon size={20}/>
                    </Link>
                </Menu.Item>
                <Menu.Item key='events' tabIndex={1}>
                    <Link to={ROUTE_EVENTS} className={`menu-item`}>
                        <CalendarTodoFillIcon size={20}/>
                    </Link>
                </Menu.Item>
                <Menu.Item key='analytics' tabIndex={2}>
                    <Link to={ROUTE_ANALYTICS} className={`menu-item`}>
                        <BarChartFillIcon size={20}/>
                    </Link>
                </Menu.Item>
                <SubMenu key='operation' tabIndex={3} title={<SlideshowFillIcon size={20} />}>
                    <Menu.Item key='meetingScheduler' tabIndex={3}>
                        <Link to={ROUTE_OPERATION('meetingScheduler')}>
                            <span className='nav-text'>{i18n.MEETING_SCHEDULER}</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='categoritesManager' tabIndex={3}>
                        <Link to={ROUTE_OPERATION('categoritesManager')}>
                            <span className='nav-text'>{i18n.CATEGORIES_MANAGER}</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='campaign' tabIndex={3}>
                        <Link to={ROUTE_OPERATION('campaign')}>
                            <span className='nav-text'>{i18n.CAMPAIGN}</span>
                        </Link>
                    </Menu.Item>
                </SubMenu>
                
                <Menu.Item key='settings'  tabIndex={4}>
                    <Link to={ROUTE_SETTINGS} className={`menu-item`}>
                        <Settings4FillIcon size={20}/>
                    </Link>
                </Menu.Item>

            </Menu>

            <Menu selectedKeys={selectedMenu} className='bottom_menu'>
                <Menu.Item key='help'>
                    <Link to={ROUTE_HELP} className={`menu-item`} >
                        <QuestionLineIcon size={20}/>
                    </Link>
                </Menu.Item>
                <Menu.Item key='right' onClick={handleChageFloat}>
                    <a href="#">
                        <ArrowRightSLineIcon size={20}/>
                    </a>
                </Menu.Item>
                
            </Menu>
        </div>
    );
};

export default Sider;