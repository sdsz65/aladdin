import React from 'react';
import Cards from './Cards';
import Trees from './Trees';

const Content = () => {
    return (
        <div className='categories-content'>
            <Cards />
            <Trees />
        </div>
    )
};

export default Content;