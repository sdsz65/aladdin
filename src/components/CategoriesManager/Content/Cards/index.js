import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { cardsInfo } from 'constants/cards';
import Card from 'components/UI/Card';
import { setSelectedCardAction } from 'store/Category/Action';
import ArrowLeftSLineIcon from 'remixicon-react/ArrowLeftSLineIcon';
import ArrowRightSLineIcon from 'remixicon-react/ArrowRightSLineIcon';

const cardsContentWidth = (17.1 * cardsInfo.length) - 2.1;
const Cards = () => {
    const dispatch = useDispatch();
    const contentCardRef = useRef(null);
    const contentRef = useRef(null);
    const { selectedCard } = useSelector(state => state.category);
    const handleClick = (cardKey) => {
        dispatch(setSelectedCardAction(cardKey));
    };

    const handleNavigate = (direction) => {
        const parentWidth = contentRef.current?.offsetWidth || 0;
        const contentWidth = contentCardRef.current?.offsetWidth;
        const currentLeft = (contentCardRef.current?.style?.left || 0).toString().replace('rem','');
        let nextLeftValue = Number(currentLeft);
        if (direction === 'left' && currentLeft < 0) {
            nextLeftValue += 8;
            if (nextLeftValue > 0) nextLeftValue = 0;
        } else if (direction === 'right' && currentLeft > ((parentWidth - contentWidth)/10)) {
            nextLeftValue -= 8;
            if (nextLeftValue < ((parentWidth - contentWidth)/10)) {
                nextLeftValue = (parentWidth - contentWidth)/10;
            }
        }
        contentCardRef.current.style.left = nextLeftValue + 'rem';
    };

    return (
        <div className='cards-block-outer'>
            <div className='cards-block-inner' ref={contentRef}>
                <div 
                    className='cards-content' 
                    ref={contentCardRef} 
                    style={{width: cardsContentWidth + 'rem'}}
                >
                    {
                        cardsInfo.map(item =>
                            <Card 
                                key={item.key}
                                data={item} 
                                selected={item.key === selectedCard} 
                                onClick={(cardKey) => handleClick(cardKey)}
                            />
                        )
                    }
                </div>
            </div>
            <div className='cards-navigator cards-navigator-right' onClick={() => handleNavigate('right')}>
                <ArrowRightSLineIcon size={18} align="center" />
            </div>
            <div className='cards-navigator cards-navigator-left' onClick={() => handleNavigate('left')}>
                <ArrowLeftSLineIcon size={18} />
            </div>
        </div>
    )
};

export default Cards;