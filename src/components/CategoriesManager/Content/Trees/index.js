import React from 'react';
import { useSelector } from 'react-redux';
import { Row, Col } from 'antd';
import { i18n } from 'config';
import CustomTree from 'components/UI/Tree';
import { 
    createAgricultralMachineryAction,
    createLivestockAction,
    createIrrigationSystemAction
} from 'store/Category/Action';

const Trees = () => {
    const { 
        agriculturalMachineryOptions,
        livestockOptions,
        irrigationSystemOptions
    } = useSelector(state => state.category);
    return (
        <Row className='trees'>
            <Col xs={24} sm={12} md={6}>

            </Col>
            <Col xs={24} sm={12} md={18}>
                <Row>
                    <Col xs={24} sm={12} md={8}>
                        <CustomTree 
                            title={i18n.AGRICULTURAL_MACHINERY_TREE_TITLE}
                            options={agriculturalMachineryOptions} 
                            createOptionAction={createAgricultralMachineryAction}
                        />
                    </Col>
                    <Col xs={24} sm={12} md={8}>
                        <CustomTree 
                            title={i18n.POULTRY_AND_LIVESTOCK}
                            options={livestockOptions} 
                            createOptionAction={createLivestockAction}
                        />
                        <CustomTree 
                            title={i18n.IRRIGATION_SYSTEMS}
                            options={irrigationSystemOptions} 
                            createOptionAction={createIrrigationSystemAction}
                        />
                    </Col>
                    <Col xs={24} sm={12} md={8}></Col>
                </Row>
            </Col>
        </Row>
    );
};

export default Trees;