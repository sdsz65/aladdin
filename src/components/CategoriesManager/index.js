import React from 'react';
import Header from './Header';
import Content from './Content';

const CategoritesManager = () => {
    return (
        <div className='categories-manager'>
            <Header />
            <Content />
        </div>
    );
};

export default CategoritesManager;