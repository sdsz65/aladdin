import React from 'react';
import { Row, Col } from 'antd';
import { i18n } from 'config';
import SearchInput from 'components/UI/Search';

const Header = () => {
    return (
        <Row className='categories-header'>
            <Col xs={24} sm={12}>
                <h3>{i18n.CATEGORIES_MANAGER}</h3>
            </Col>
            <Col xs={24} sm={12}>
                <SearchInput />
            </Col>
        </Row>
    );
};

export default Header;