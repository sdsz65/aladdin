import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Tabs } from 'antd';
import { i18n } from 'config';
import { ROUTE_OPERATION } from 'routes';
import Breadcrumb from './Breadcrumb';
import CategoritesManager from 'components/CategoriesManager';

const { TabPane } = Tabs;
const OperationTabs = () => {
    const navigate = useNavigate();
    const { tab } = useParams();
    const [activeTab, setActiveTab] = useState(tab);
    useEffect(() => {
        setActiveTab(tab);
    }, [tab])

    const handleChangeTab = (tab) => {
        setActiveTab(tab);
        navigate(ROUTE_OPERATION([tab]));
    };

    return (
        <Tabs 
            className='tabs' 
            activeKey={activeTab} 
            onChange={handleChangeTab}
        >
            <TabPane tab={i18n.MEETING_SCHEDULER} key='meetingScheduler'>
                <Breadcrumb title={i18n.MEETING_SCHEDULER} />
                <div>{i18n.MEETING_SCHEDULER}</div>
            </TabPane>
            <TabPane tab={i18n.CATEGORIES_MANAGER} key='categoritesManager'>
                <Breadcrumb title={i18n.CATEGORIES_MANAGER} />
                <CategoritesManager />
            </TabPane>
            <TabPane tab={i18n.CAMPAIGN} key='campaign'>
                <Breadcrumb title={i18n.CAMPAIGN} />
                <div>{i18n.CAMPAIGN}</div>
            </TabPane>
        </Tabs>
    );
};

export default OperationTabs;