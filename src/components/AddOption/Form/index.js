import React, { useEffect, useCallback } from 'react';
import { Formik } from 'formik';
import { useDispatch } from 'react-redux';
import { message } from 'antd';
import { i18n } from 'config';
import useFormSubmitAction from 'customHooks/useFormSubmitAction';
import FormRender from '../FormRender';

const initialValues = {
    name: '',
};

const Form = ({hideModal, createOptionAction}) => {
    const dispatch = useDispatch();

    const [
        submitStatus,
        setSubmitStatus,
        formAction,
        setFormAction,
    ] = useFormSubmitAction(false);

    const handleSubmit = (data, action) => {
        setFormAction(action);
        setSubmitStatus(true);
        dispatch(createOptionAction(data.name));
    };

    const handleResetResult = useCallback((cb) => {
        setSubmitStatus(false);
        cb && cb();
    }, [setSubmitStatus, dispatch]);

    const handleSubmitResult = useCallback(() => {
        if (submitStatus) {
            message.success(i18n.CREATE_OPTION_MSG, 2)
            handleResetResult(() => {
                hideModal();
            });
        }
    }, [ submitStatus, handleResetResult, hideModal ]);

    useEffect(() => {
        handleSubmitResult();
    }, [handleSubmitResult]);

    return (
        <Formik
            enableReinitialize
            initialValues={initialValues}
            onSubmit={handleSubmit}
            setFieldValue
            render={(formikProps) => (
                <FormRender {...formikProps} hideModal={hideModal} />
            )}
        />
    );
};

export default Form;