import React from 'react';
import { Field, Form } from 'formik';
import { Button, Col, Row } from 'antd';
import { i18n } from 'config';
import { isRequired } from 'utils/FieldValidators/isRequired';
import { Input } from 'components/UI/Fields';

const FormicForm = (props) => {
    const {
        handleSubmit,
        submitCount,
        isSubmitting,
        hideModal
    } = props;

    return (
        <Form onSubmit={handleSubmit}>
            <Field
                label={i18n.OPTION_NAME}
                component={Input}
                name="name"
                type="text"
                isrequired={true}
                validate={isRequired}
                submitCount={submitCount}
            />
            <Row type="flex" justify="end" align="middle">
                <Col>
                    <Button onClick={hideModal} disabled={isSubmitting}>
                        {i18n.CANCEL}
                    </Button>
                </Col>
                <Col>
                    <Button
                        htmlType="submit"
                        type="primary"
                        loading={isSubmitting}
                        disabled={isSubmitting}
                    >
                        {i18n.ADD}
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default FormicForm;