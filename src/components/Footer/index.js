import { i18n } from 'config';
import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTE_PRIVACY_POLICY, ROUTE_SUPPORT, ROUTE_TERMS_OF_USE } from 'routes';

const Footer = () => {
    return (
        <footer className="footer">
            <span className='copyright'>{i18n.COPYRIGHT}</span>
            <ul>
                <li><Link to={ROUTE_TERMS_OF_USE}>{i18n.TERMS_OF_USE}</Link></li>
                <li><Link to={ROUTE_PRIVACY_POLICY}>{i18n.PRIVACY_POLICY}</Link></li>
                <li><Link to={ROUTE_SUPPORT}>{i18n.SUPPORT}</Link></li>
            </ul>
        </footer>
    );
};

export default Footer;