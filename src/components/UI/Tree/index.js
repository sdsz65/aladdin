import React, { useState } from 'react';
import { Tree, Modal } from 'antd';
import { i18n } from 'config';
import AddCircleLineIcon from 'remixicon-react/AddCircleLineIcon';
import CreateOptionForm from 'components/AddOption/Form';

const CustomTree = ({options, createOptionAction, title}) => {
    const [visible, setVisible] = useState(false);

    const showModal = () => {
        setVisible(true);
    };

    const hideModal = () => {
        setVisible(false);
    };

    return (
        <>
            <Modal
                destroyOnClose
                onCancel={hideModal}
                footer={null}
                handleClose={hideModal}
                getContainer={document.getElementById('modal-root')}
                title={i18n.ADD_OPTION}
                visible={visible}
            >
                <CreateOptionForm 
                    hideModal={hideModal} 
                    createOptionAction={createOptionAction}
                />
            </Modal>
            <Tree
                className='custome-tree tree-without-indent'
                checkable
                selectable={false}
                defaultExpandAll
                treeData={[ {
                    title: title,
                    key: '0',
                    children: options.reduce((data, item, key) => {
                        return ([...data, {
                            title: item,
                            key: item + key 
                        }]);
                    }, [])
                }]}
            />
            <div className='add-option' onClick={showModal}>
                <AddCircleLineIcon />
                <span>{i18n.ADD_YOUR_OPTION}</span>
            </div>
        </>
    );
};

export default CustomTree;