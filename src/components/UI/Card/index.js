import React from 'react';

const Card = ({data, selected, onClick}) => {
    return (
        <div 
            className={`card-item ${selected ? 'selected-card' : ''}`}
            onClick={() => onClick(data.key)}
        >
            <img src={data.icon} alt={data.key} />
            <span>{data.title}</span>
        </div>
    )
};

export default Card;