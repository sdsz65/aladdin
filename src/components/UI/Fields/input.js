import React from 'react';
import { Form, Col, Row, Input } from 'antd';
const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {span: 24},
    wrapperCol: {span: 24},
};

const InputField = () => ({
  field,
  form,
  label,
  submitCount,
  ...props
}) => {
  const touched = form.touched[field.name];
  const submitted = submitCount > 0;
  const hasError = form.errors[field.name];
  const submittedError = hasError && submitted;
  const touchedError = hasError && touched;

  const onInputChange = ({ target: { value } }) => {
    if (form && form.setFieldValue) {
      form.setFieldValue(field.name, value);
    }
  };

  return (
    <div className='field-container'>
      <FormItem
        {...formItemLayout}
        label={
          <span>
            {label}
            {props.isrequired ? <span style={{ color: 'red' }}>&nbsp;*&nbsp;</span>: ''}
          </span>
        }
        validateStatus={submittedError || touchedError ? 'error' : 'success'}
        help={submittedError || touchedError ? hasError : ''}
        className='ltrInput'
      >
        <Row>
          <Col flex="auto">
            <Input
              autoComplete="new-password"
              {...field}
              {...props}
              value={field.value}
              type='text'
              onChange={onInputChange}
            />
          </Col>
        </Row>
      </FormItem>
    </div>
  );
};

export default InputField;