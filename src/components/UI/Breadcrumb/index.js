import React from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb } from 'antd';

const BreadcrumbConfig = ({config}) => {
  if (!config || config.length < 1) return null;

  let content = config.map((item, index) => {
    let content = null;
    if (item.externalLink) {
      content = (
        <a href={item.link} target={item.linkTarget}>
          {item.icon ? item.icon : null}&nbsp;{item.title}
        </a>
      );
    } else {
      if (item.link) {
        content = (
          <Link to={item.link}>
            {item.icon ? item.icon : null}&nbsp;{item.title}
          </Link>
        );
      } else {
        if (item.title) {
          content = item.icon ? (
            <span>
              {item.icon}&nbsp;{item.title}
            </span>
          ) : (
            <span>{item.title}</span>
          );
        } else {
          content = item.icon || null;
        }
      }
    }

    return <Breadcrumb.Item key={index}>{content}</Breadcrumb.Item>;
  });

  return <Breadcrumb style={{ marginBottom: '2rem' }}>{content}</Breadcrumb>;
}

export default BreadcrumbConfig;
