import React from 'react';
import SearchLineIcon from 'remixicon-react/SearchLineIcon';

const SearchInput = () => {
    return (
        <div className='search-block'>
            <input type="text" placeholder='Search'/>
            <SearchLineIcon />
        </div>
    );
};

export default SearchInput;