import { useEffect, useState } from 'react';

function useFormSubmitAction(defaultValue) {
  const [submitStatus, setSubmitStatus] = useState(defaultValue);
  const [formAction, setFormAction] = useState(null);

  useEffect(() => {
    formAction && formAction.setSubmitting(submitStatus);
  }, [formAction, submitStatus]);
  return [submitStatus, setSubmitStatus, formAction, setFormAction];
}

export default useFormSubmitAction;
