import { i18n } from "config";
import detergentIcon from 'assets/images/detergent.png';
import craneIcon from 'assets/images/crane.png';
import suppliersIcon from 'assets/images/suppliers.png';
import truckIcon from 'assets/images/truck.png';
import medicalIcon from 'assets/images/medical.png';

export const cardsInfo = [
    {
        key: 'material',
        title: i18n.MATERIAL,
        icon: detergentIcon
    },
    {
        key: 'equipment',
        title: i18n.EQUIPMENT,
        icon: craneIcon
    },
    {
        key: 'components',
        title: i18n.COMPONENTS,
        icon: suppliersIcon
    },
    {
        key: 'construction',
        title: i18n.CONSTRUCTION,
        icon: truckIcon
    },
    {
        key: 'medical',
        title: i18n.MEDICAL,
        icon: medicalIcon
    },
    {
        key: 'food1',
        title: i18n.FOOD,
        icon: detergentIcon
    },
    {
        key: 'food2',
        title: i18n.FOOD,
        icon: detergentIcon
    },
    {
        key: 'food3',
        title: i18n.FOOD,
        icon: detergentIcon
    },
    {
        key: 'food4',
        title: i18n.FOOD,
        icon: detergentIcon
    }
];