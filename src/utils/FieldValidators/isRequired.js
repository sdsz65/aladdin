import { i18n } from 'config';

export const isRequired = (value) => {
  return !value ? i18n.FIELD_IS_REQUIRED : '';
};